#!/bin/sh
set -e

psql -v ON_ERROR_STOP=1 --username "$POSTGRES_USER" --dbname "$POSTGRES_DB" <<-EOSQL
    CREATE USER admin PASSWORD '${ADMIN_USER_PASSWORD}';
    CREATE EXTENSION IF NOT EXISTS pg_trgm;
EOSQL
